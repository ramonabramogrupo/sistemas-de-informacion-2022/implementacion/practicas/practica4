﻿DROP DATABASE IF EXISTS practica4; 
CREATE DATABASE practica4;
USE practica4; 

CREATE TABLE empleado(
  `nss-empleado` varchar(10),
  nombre varchar(100),
  apellido varchar(200),
  iniciales varchar(4),
  fecha_ncto date,
  sexo varchar(20),
  direccion varchar(400),
  salario float,
  `nombre-d pertenece` varchar(100),
  PRIMARY KEY(`nss-empleado`)
  );

CREATE TABLE supervisa(
  `nss-empleado` varchar(10),
  `nss-supervisor` varchar(10),
  PRIMARY KEY(`nss-empleado`,`nss-supervisor`),
  UNIQUE KEY (`nss-empleado`)
  );

CREATE TABLE dependiente(
  `nombre-dependiente` varchar(100),
  `nss-empleado`varchar(10),
  sexo varchar(20),
  fecha_ncto date,
  parentesco varchar(100),
  PRIMARY KEY(`nombre-dependiente`,`nss-empleado`)
  );

CREATE TABLE departamento(
  `nombre-d` varchar(100),
  `numero-d` int,
  numdeempleados int,
  `nss-empleado-dirige` varchar(10),
  `fecha-inicio-jefe` date,
  PRIMARY KEY(`nombre-d`,`numero-d`),
  UNIQUE KEY (`nss-empleado-dirige`)
  );

CREATE TABLE localizaciones(
  `nombre-d`varchar(100),
  `numero-d` int,
  `localizacion-dept` varchar(100),
  PRIMARY KEY(`nombre-d`,`numero-d`,`localizacion-dept`)
  );

CREATE TABLE proyecto(
  numerop int,
  nombrep varchar(100),
  localizacion varchar(200),
  `nombre-d-controla` varchar(100),
  `numero-d-controla` int,
  PRIMARY KEY(numerop,nombrep)
  );

CREATE TABLE trabaja_en(
  `nss-empleado` varchar(10),
  `nombre-p` varchar(100),
  `numero-p` int,
  horas int,
  PRIMARY KEY(`nss-empleado`,`nombre-p`,`numero-p`)
  );

    
-- claves ajenas

ALTER TABLE empleado
  ADD COLUMN `numero-d pertenece` int,
  ADD CONSTRAINT fkEmpleadoDepartamento
    FOREIGN KEY (`nombre-d pertenece`,`numero-d pertenece`)
    REFERENCES departamento(`nombre-d`,`numero-d`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE supervisa  
  ADD CONSTRAINT fkSupervisaEmpleado 
    FOREIGN KEY (`nss-empleado`) REFERENCES empleado(`nss-empleado`)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkSupervisaEmpleado1 
    FOREIGN KEY (`nss-supervisor`) REFERENCES empleado(`nss-empleado`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE dependiente
  ADD CONSTRAINT fkDependienteEmpleado
    FOREIGN KEY (`nss-empleado`) REFERENCES empleado(`nss-empleado`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE departamento 
  ADD  CONSTRAINT fkDepartamentoEmpleado
    FOREIGN KEY (`nss-empleado-dirige`) REFERENCES empleado(`nss-empleado`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE localizaciones
  ADD CONSTRAINT fkLocalizacionesDepartamento
    FOREIGN KEY (`nombre-d`,`numero-d`) 
    REFERENCES departamento(`nombre-d`,`numero-d`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE proyecto
  ADD  CONSTRAINT fkProyectoDepartamento
    FOREIGN KEY (`nombre-d-controla`,`numero-d-controla`) 
    REFERENCES departamento(`nombre-d`,`numero-d`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE trabaja_en
  ADD  CONSTRAINT fkTrabaja_enEmpleado 
    FOREIGN KEY (`nss-empleado`)
    REFERENCES empleado(`nss-empleado`)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkTrabaja_enProyecto
    FOREIGN KEY (`numero-p`,`nombre-p`)
    REFERENCES proyecto(numerop,nombrep)
    ON DELETE RESTRICT ON UPDATE RESTRICT;