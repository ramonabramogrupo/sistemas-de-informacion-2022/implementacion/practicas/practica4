﻿USE practica4;

INSERT INTO empleado 
  (`nss-empleado`, nombre, apellido, iniciales, fecha_ncto, sexo, direccion, salario, `nombre-d pertenece`, `numero-d pertenece`) VALUES 
  ('1', 'eva', 'gutierrez', 'eg', NULL , 'mujer', NULL , 1000, NULL , NULL );

SELECT * FROM empleado;

INSERT INTO departamento 
  (`nombre-d`, `numero-d`, numdeempleados, `nss-empleado-dirige`, `fecha-inicio-jefe`)  VALUES 
  ('ventas', 1, 0, '1', '2020/1/5');

SELECT * FROM departamento d;

INSERT INTO empleado 
  (`nss-empleado`, nombre, apellido, iniciales, fecha_ncto, sexo, direccion, salario, `nombre-d pertenece`, `numero-d pertenece`) VALUES 
  ('2', 'Jorge', 'Abascal', 'ja', NULL, 'varon', NULL , 1100, 'ventas', 1),
  ('3','Luisa','Gomez','Lg',NULL,NULL,NULL,NULL,'ventas',1),
  ('4','Roberto','Lopez','Rl',NULL,NULL,NULL,NULL,NULL,NULL);
 

SELECT * FROM empleado e;

INSERT INTO departamento (`nombre-d`, `numero-d`, numdeempleados, `nss-empleado-dirige`, `fecha-inicio-jefe`)
  VALUES ('ventas', 2, 0, '4', CURDATE());

SELECT * FROM departamento d;

INSERT INTO empleado (`nss-empleado`, nombre, apellido, iniciales, fecha_ncto, sexo, direccion, salario, `nombre-d pertenece`, `numero-d pertenece`)
  VALUES 
   ('5','Maria','Loma','Ml',NULL,NULL,NULL,NULL,'ventas',2);

SELECT * FROM empleado e;

INSERT INTO supervisa 
  (`nss-empleado`, `nss-supervisor`) VALUES 
  ('2', '4'),
  ('3','4');

SELECT * FROM supervisa s;

INSERT INTO dependiente 
  (`nombre-dependiente`, `nss-empleado`, sexo, fecha_ncto, parentesco) VALUES 
  ('jose angel', '1', 'hombre', '1989/10/29', 'hijo'),
  ('ana isabel','1','mujer',NULL,'hija');

SELECT * FROM dependiente d;

INSERT INTO localizaciones 
  (`nombre-d`, `numero-d`, `localizacion-dept`) VALUES 
  ('ventas', 1, 'Madrid'),
  ('ventas',2,'Santander');

SELECT * FROM localizaciones l;

INSERT INTO proyecto 
  (numerop, nombrep, localizacion, `nombre-d-controla`, `numero-d-controla`) VALUES 
  (1, 'gestion residuos sardinero', 'Santander','ventas', 1),
  (1, 'Reparacion mesas puertochico','Santander','ventas',2);

SELECT * FROM proyecto p;


INSERT INTO trabaja_en 
  (`nss-empleado`, `nombre-p`, `numero-p`, horas) VALUES 
  ('1', 'gestion residuos sardinero', 1, 10),
  ('2', 'Reparacion mesas puertochico',1,20);

SELECT * FROM trabaja_en te;

